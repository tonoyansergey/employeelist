<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 4/24/2019
  Time: 17:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
<head>
    <title>Employees</title>
</head>
<body>
    <table>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Age</th>
            <th>Salary</th>
        </tr>
            <c:forEach items="${employeesFromRep}" var="employee">
            <tr>
                <td>${employee.firstName}</td>
                <td>${employee.lastName}</td>
                <td>${employee.age}</td>
                <td>${employee.salary}</td>
            </tr>
            </c:forEach>
    </table>
    <br>
    <a href="addEmployee">Add new employee</a>

</body>
</html>
