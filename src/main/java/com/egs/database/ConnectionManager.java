package com.egs.database;

import org.springframework.jdbc.datasource.DriverManagerDataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionManager {

    private static ConnectionManager instance;
    private static Connection connection;
    private static DriverManagerDataSource dataSource;

    private String dbUrl;
    private String dbUserName;
    private String dbPassword;
    private String driverClassName;

    public ConnectionManager() {

        loadProperties();

    }

    private void loadProperties () {

        Properties properties = new Properties();
        try (InputStream inStream = ConnectionManager.class.getClassLoader().getResourceAsStream("db.properties")) {

            properties.load(inStream);

            dbUrl = properties.getProperty("db.url");
            dbUserName = properties.getProperty("db.username");
            dbPassword = properties.getProperty("db.password");
            driverClassName = properties.getProperty("db.driverClassName");

        } catch (IOException ioe) {
            throw new IllegalStateException(ioe);
        }
    }

    public static ConnectionManager getInstance() {
        if (instance == null) {
            synchronized (ConnectionManager.class) {
                if (instance == null) {
                    instance = new ConnectionManager();
                }
            }
        }
        return instance;
    }

    public Connection getConnection() {
        try {
            if (connection == null || connection.isClosed()) {
                try {
                    Class.forName(driverClassName);
                    connection = java.sql.DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
                } catch (ClassNotFoundException | SQLException e) {
                    e.printStackTrace();
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private void setDataSource() {
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(dbUserName);
        dataSource.setPassword(dbPassword);
        dataSource.setDriverClassName(driverClassName);
    }

    public DriverManagerDataSource getDataSource() {
        if (dataSource == null) {
            synchronized (ConnectionManager.class) {
                if (dataSource == null) {
                    dataSource = new DriverManagerDataSource();
                    setDataSource();
                }
            }
        }
        return dataSource;
    }
}
