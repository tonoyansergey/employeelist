package com.egs.dao;

import com.egs.models.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
public class EmployeesDaoJdbcTemplateImpl implements EmployeeDao {

    @Autowired
    private JdbcTemplate template;

    //language=SQL
    private final String SQL_SELECT_ALL = "SELECT * FROM employee_db.employee";

    //language=SQL
    private final String SQL_SELECT_ALL_BY_FIRST_NAME = "SELECT * FROM employee_db.employee WHERE firstName=?";

    //language=SQL
    private final String SQL_INSERT = "INSERT INTO employee_db.employee (firstName, lastName, age, salary) VALUES (?, ?, ?, ?)";

    private RowMapper<Employee> rowMapper = new RowMapper<Employee>() {
        @Override
        public Employee mapRow(ResultSet resultSet, int i) throws SQLException {
            return new Employee(
                    resultSet.getInt("id"),
                    resultSet.getString("firstName"),
                    resultSet.getString("lastName"),
                    resultSet.getInt("age"),
                    resultSet.getInt("salary"));
        }
    };

    @Override
    public List<Employee> findByFirstName(String firstName) {
        return template.query(SQL_SELECT_ALL_BY_FIRST_NAME, rowMapper, firstName);
    }

    @Override
    public Employee find(int id) {
        return null;
    }

    @Override
    public void save(Employee model) {
        template.update(SQL_INSERT, model.getFirstName(), model.getLastName(), model.getAge(), model.getSalary());
    }

    @Override
    public void update(Employee model) {

    }

    @Override
    public void delete(int id) {

    }

    @Override
    public List<Employee> findAll() {
        return template.query(SQL_SELECT_ALL, rowMapper);
    }
}
