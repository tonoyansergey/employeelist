package com.egs.dao;

import com.egs.models.Employee;

import java.util.List;

public interface EmployeeDao extends CrudDao<Employee> {

    List<Employee> findByFirstName(String firstName);
}
