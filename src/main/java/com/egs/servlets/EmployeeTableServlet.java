package com.egs.servlets;

import com.egs.config.AppConfig;
import com.egs.config.AppContextLoader;
import com.egs.dao.EmployeeDao;
import com.egs.database.ConnectionManager;
import com.egs.models.Employee;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/")
public class EmployeeTableServlet extends HttpServlet {

    private ConnectionManager connectionManager;
    private EmployeeDao employeeDao;

    @Override
    public void init() throws ServletException {

//        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

        employeeDao = AppContextLoader.getContext().getBean(EmployeeDao.class);

    }



    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<Employee> employeeList = employeeDao.findAll();
        req.setAttribute("employeesFromRep", employeeList);
        RequestDispatcher requestDispatcher = req.getServletContext().getRequestDispatcher("/jsp/index.jsp");
        requestDispatcher.forward(req, resp);

    }


}
