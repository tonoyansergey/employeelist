package com.egs.servlets;

import com.egs.config.AppConfig;
import com.egs.config.AppContextLoader;
import com.egs.dao.EmployeeDao;
import com.egs.models.Employee;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addedSuccessfully")
public class PageRedirectServlet extends HttpServlet {

    private EmployeeDao employeeDao;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        int age = Integer.parseInt(req.getParameter("age"));
        int salary = Integer.parseInt(req.getParameter("salary"));

        Employee employee = new Employee(firstName, lastName, age, salary);

        employeeDao.save(employee);

        resp.sendRedirect("/");
    }


    @Override
    public void init() throws ServletException {

        employeeDao = AppContextLoader.getContext().getBean(EmployeeDao.class);
    }
}
