package com.egs.config;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AppContextLoader {

    private static ApplicationContext context;

    public static ApplicationContext getContext() {
        if (context == null) {
            synchronized (AppContextLoader.class) {
                if (context == null) {
                    context = new AnnotationConfigApplicationContext(AppConfig.class);

                }
            }
        }
        return context;
    }
}
