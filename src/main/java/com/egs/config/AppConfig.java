package com.egs.config;

import com.egs.database.ConnectionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
@ComponentScan(basePackages = "com.egs")
public class AppConfig {

    @Bean
    public JdbcTemplate template() {
        return new JdbcTemplate(dataSource());
    }

    @Bean
    public ConnectionManager connectionManager () {
        return new ConnectionManager();
    }

    @Bean
    public DataSource dataSource() {
        return connectionManager().getDataSource();
    }
}
